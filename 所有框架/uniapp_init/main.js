import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'

//配置拦截器
uni.addInterceptor("request", {
	//invoke请求发送出去之前触发的效果
	invoke(config) {
		console.log("接口发起请求");
		console.log(config);
		const baseUrl = "https://api-hmugo-web.itheima.net/api/public/v1/";
		//请求接口地址的拼接
		config.url = baseUrl + config.url;
		
		//loading加载修购
		uni.showLoading({
			title:"努力加载中"
		})
	},

	complete() {
		console.log("请求回调成功触发");
		
		uni.hideLoading();//隐藏效果
	}
})


const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
