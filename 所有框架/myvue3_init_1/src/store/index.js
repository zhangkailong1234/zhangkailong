//1. 先导入vuex的内容
import {createStore} from "vuex";

import vuexPersist from "vuex-persist";
const vuexLocal = new vuexPersist({
    storage:window.localStorage,
})

//创建vuex对象的值即可
export default createStore({
    state:{
        
    },
    mutations:{
       
    },
    getters:{},
    plugins:[vuexLocal.plugin]
})