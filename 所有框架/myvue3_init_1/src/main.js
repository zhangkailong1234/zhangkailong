import { createApp } from 'vue'
import App from './App.vue'

//导入路由
import router from "./router/index";
//导入vuex的内容
import store from "./store/index";

//导入elementPlus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

//导入axios的信息
import axios from "./utils/request";
const app = createApp(App);
app.config.globalProperties.$axios = axios;

app.use(router).use(store).use(ElementPlus).mount('#app');
