//导入路由的对象值
import {createRouter,createWebHashHistory} from "vue-router";

export default createRouter({
    //定义路由模式
    history: createWebHashHistory(),
    //定义路由的地址信息
    routes:[
        {
            path:"/",
            name:"home",
            component:() => import("../views/Home.vue"),
        }
    ]
})