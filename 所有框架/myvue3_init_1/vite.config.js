import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  //配置服务端的端口信息
  server: {
    port: 8081,
    open: true, // 自动打开浏览器
    proxy: { // 配置前端跨域

    }
  },
  plugins: [vue()]
})
